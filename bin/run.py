#!/usr/bin/python

import common
import time
import os
import json

NUM_CLIENTS = 1
NUM_GROUPS = 2
NUM_ACCEPTOR = 3
NUM_REPLICA = 2

NODE_BEGIN = 2
NODE_END = 40
NODE_EXCLUDE = [1, 68, 73, 71, 41, 75, 78, 63, 80]

NUM_PAXOS_PROCESS_PER_NODE = 2
NUM_LEARNER_PROCESS_PER_NODE = 1

MCAST_GLOBAL_RATE = 0

SENSE_DURATION = 60
localhosts = []

log_path = os.path.normpath(
    common.script_dir() + '/logs/' + str(NUM_CLIENTS) + "c_" + str(NUM_GROUPS) + "g_" + str(NUM_REPLICA) + "r_" +
    str(MCAST_GLOBAL_RATE) + "a")

gatherer_node = 'localhost' if common.LOCALHOST else 'node30'
client_node = 'localhost' if common.LOCALHOST else 'node31'

receiver_class = "ch.usi.dslab.bezerra.mcad.examples.RidgeReceiver"
sender_class = "ch.usi.dslab.bezerra.mcad.examples.RidgeSender"

SYSTEM_FILE = common.script_dir() + '/system_config/system_' + str(NUM_GROUPS) + 'g' + str(NUM_REPLICA) + "p.json"

if common.LOCALHOST: SYSTEM_FILE = common.script_dir() + "/system_config/ridge_2g3e.json"
config_stream = open(SYSTEM_FILE)
config = json.load(config_stream)

ridge_deployer = common.script_dir() + "/../src/main/java/ch/usi/dslab/bezerra/mcad/examples/deployMcast.py "
deployment_cmd = ridge_deployer + SYSTEM_FILE


def run2p():
    print 2


def run():
    os.system(deployment_cmd)

    cmdList = []
    for member in config["group_members"]:
        pid = member["pid"]
        group = member["group"]
        host = member["host"]
        port = member["port"]
        launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH, receiver_class, pid, group, SYSTEM_FILE, NUM_GROUPS]
        launchNodeCmdString += [gatherer_node, common.SENSE_GATHERER_PORT, SENSE_DURATION, log_path]
        launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
        cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    # # deploy gatherer
    logsargs = ['throughput', 'ridge_learner', str(NUM_REPLICA), 'throughput', 'ridge_client', str(1), 'latency',
                'ridge_client', str(1)]
    logsargs = " ".join([str(val) for val in logsargs])
    launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.SENSE_CLASS_GATHERER,
                           common.SENSE_GATHERER_PORT, common.SENSE_GATHERER_PATH, logsargs]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": gatherer_node, "cmdstring": launchNodeCmdString})

    # deploy client
    launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, sender_class, SYSTEM_FILE, 1, "false",
                           "false" if MCAST_GLOBAL_RATE == 0 else "true",
                           gatherer_node, common.SENSE_GATHERER_PORT, SENSE_DURATION, log_path]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": gatherer_node, "cmdstring": launchNodeCmdString})

    thread = common.LauncherThread(cmdList)
    thread.start()
    thread.join()


if __name__ == '__main__':
    # if NUM_GROUPS == 1:
    #     run1p()
    # elif NUM_GROUPS == 2:
    #     run2p()
    run()
