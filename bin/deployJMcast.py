import inspect
import json
import os
import socket
import sys
import threading

LOCALHOST = False

if socket.gethostname() in ['longs-usi-mbp.mobile.usilu.net', 'longs-mbp.mobile.usilu.net',
                            "Longs-MBP.fritz.box"]:
    LOCALHOST = True

REMOTE_ENV = "" if LOCALHOST else "LD_LIBRARY_PATH=/home/long/.local/lib"


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def sshcmdbg(node, cmdstring):
    cmd = "ssh -o StrictHostKeyChecking=no " + node + " " + REMOTE_ENV + " \"" + cmdstring + "\" &"
    os.system(cmd)


class LauncherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        for cmd in self.cmdList:
            sshcmdbg(cmd["node"], cmd["cmdstring"])


GLOBAL_HOME = os.path.normpath(script_dir() + '/../../../../../../../../../../')
LIBMCAST_HOME = os.path.normpath(GLOBAL_HOME + '/libjmcast')
LIBMCAST_CP = os.path.normpath(LIBMCAST_HOME + '/target/classes')
LIBMCAST_PAXOS_PROCESS = os.path.normpath(LIBMCAST_HOME + '/libmcast/build/local/bin/proposer-acceptor')

# sleep(0.5)


if len(sys.argv) < 2:
    print("Usage: " + sys.argv[0] + " config_file")

system_config_file = os.path.abspath(sys.argv[1])

config_json = open(system_config_file)

config = json.load(config_json)

paxos_groups = config["paxos_groups"]
paxos_config_file = config["paxos_config_file"]

cmdList = []

for group in paxos_groups:
    for node in group["group_members"]:
        launchNodeCmdPieces = [LIBMCAST_PAXOS_PROCESS, node["pid"], paxos_config_file + '.' + str(group["group_id"]),
                               "> /tmp/px00.log"]
        launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
        cmdList.append({"node": node['host'], "cmdstring": launchNodeCmdString})
server_thread = LauncherThread(cmdList)
server_thread.start()
server_thread.join()
