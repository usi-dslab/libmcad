#!/usr/bin/env bash

JAVA_CMD="java -XX:+UseG1GC -cp /Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/target/libmcad-git.jar -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/log4j2.xml"

SERVER_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deployReceiver.py"

CLIENT_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deploySender.py"

MCAST_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deployMcast.py"

CONFIG_FILE="/Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/ridge_1g3e.json"

$JAVA_CMD ch.usi.dslab.bezerra.sense.DataGatherer 40000 /tmp/ridge throughput learner 1 latency client 1 &

$MCAST_DEPLOYER $CONFIG_FILE

$SERVER_DEPLOYER 9 1 $CONFIG_FILE
#
#$CLIENT_DEPLOYER $CONFIG_FILE 1 false
#./deploySender.py /Users/longle/Dropbox/Workspace/PhD/DS-SMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/ridge_1g3e.json 1 false false


