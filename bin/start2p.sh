#!/usr/bin/env bash

JAVA_CMD="java -XX:+UseG1GC -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/bin/log4jDebug.xml"

CLASS_PATH="../target/classes:../../netwrapper/target/classes:../../libjmcast/target/classes:../../sense/target/classes:../../dependencies/*"

SERVER_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deployReceiver.py"

CLIENT_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deploySender.py"

MCAST_DEPLOYER="python /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/deployMcast.py"

CONFIG_FILE="/Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/examples/ridge_2g3e.json"

$JAVA_CMD -cp "$CLASS_PATH" ch.usi.dslab.bezerra.sense.DataGatherer 60000 /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/logs/gatherer throughput jmcast_receiver 3 & # throughput jmcast_client 1 latency jmcast_client 1 &

#$MCAST_DEPLOYER $CONFIG_FILE

#$JAVA_CMD ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 0 &
#$JAVA_CMD ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 1 &
#$JAVA_CMD ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 2 &
#$JAVA_CMD ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 3 &

#$SERVER_DEPLOYER 10 2 $CONFIG_FILE

#$CLIENT_DEPLOYER $CONFIG_FILE 1 false true

python /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/jmcast/JMcastPaxosDeployer.py /Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/bin/system_config/generatedSysConfig.json

RECEIVER_CLASS=ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver

$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 0 &
$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 1 &
$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 2 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 3 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 4 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 5 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 6 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 7 &
#$JAVA_CMD -cp "$CLASS_PATH" $RECEIVER_CLASS 8 &

#java -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/ScalableSMR/libmcad/bin/log4jDebug.xml -XX:+UseG1GC -cp "../target/classes:../../netwrapper/target/classes:../../sense/target/classes:../../libjmcast/target/classes:../../dependencies/*" ch.usi.dslab.bezerra.mcad.examples.JMcastSender
