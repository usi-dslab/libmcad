/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastAgent;
import ch.usi.dslab.bezerra.mcad.MulticastClient;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.ClientCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastClientJNI;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class JMcastClient implements MulticastClient {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JMcastClient.class);
    private BlockingQueue<Message> receivedReplies;
    private McastClientJNI jniClient;
    private MulticastAgent parentAgent;
    private RequestBatcher requestBatcher;
    public boolean isReady = false;
    int connectedGroups = 0;
    int groupsCount = 0;
    int clientId;


    static final int MSG_SIZE = 1000;
    static final int GROUP_COUNT = 3;
    static final Random random = new Random(); // Or SecureRandom
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


    public JMcastClient(MulticastAgent parentAgent, int clientId, String configFile) {

        this.parentAgent = parentAgent;
        this.clientId = clientId;
        groupsCount = Group.getAllGroups().size();
        receivedReplies = new LinkedBlockingQueue<>();
        jniClient = new McastClientJNI(clientId, configFile);

        requestBatcher = new RequestBatcher(this);
        requestBatcher.start();

        ClientCallbackHandler cbOnConnected = new ClientCallbackHandler() {
            @Override
            public void success(Message reply) {
                int groupId = (int) reply.getItem(1);
                int nodeId = (int) reply.getItem(2);

                // send credentials message
                logger.debug("JMcastClient {} sending credential", clientId);
                JMcastMessage credentials = new JMcastMessage(JMcastServer.MessageType.CLIENT_CREDENTIALS, clientId);
                parentAgent.multicast(Group.getGroup(groupId), credentials);

                connectedGroups += 1;
                logger.debug("JMcastClient {} connected to node {} of group {} ({},{})", clientId, nodeId, groupId, connectedGroups, groupsCount);
                if (groupsCount == connectedGroups) {
                    logger.debug("JMcastClient {} is ready", clientId);
                    isReady = true;
                }
            }

            @Override
            public void error(Message reply) {
                if (isReady) {
                    logger.error("JMcastClient {} disconnected {}", clientId, reply);
                }
                isReady = false;
            }
        };

        ClientCallbackHandler cbOnReply = reply -> {
            try {
                receivedReplies.put(reply);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        jniClient.setConnectedCallback(cbOnConnected);
        jniClient.setReplyCallback(cbOnReply);
        jniClient.start();
    }

    @Override
    public void connectToServer(int serverId) {
        int groupId = ((JMcastAgent) parentAgent).getProcessGroup(serverId).getId();
        int gpid = JMcastNode.getNode(serverId).getGid();
        logger.debug("{} is connecting to server {} () in group {}", clientId, gpid, serverId, groupId);
        jniClient.jni_connect_to_node(groupId, gpid);
    }

    public void connectToServer(int groupId, int localServerId) {
        logger.debug("{} is connecting to server {} in group {}", clientId, localServerId, groupId);
        jniClient.jni_connect_to_node(groupId, localServerId);
    }

    @Override
    public Message deliverReply() {
        try {
            return receivedReplies.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    @Override
    public void multicast(Set<Group> destinations, ClientMessage message) {
//        multicast(destinations, new Message(message));
        requestBatcher.addRequest(destinations, message);
    }


//    public void multicast(Group destination, Message message) {
//        Set<Group> destinations = new HashSet<>(1);
//        destinations.add(destination);
//        multicast(destinations, message);
//    }

    @Override
    public void connectToOneServerPerPartition() {
        // TODO Auto-generated method stub
    }

    public MulticastAgent getParentAgent() {
        return parentAgent;
    }

    public McastClientJNI getJNIClient() {
        return jniClient;
    }
}
