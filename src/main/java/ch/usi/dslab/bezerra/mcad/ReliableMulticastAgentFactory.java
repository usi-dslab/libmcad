package ch.usi.dslab.bezerra.mcad;

import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ch.usi.dslab.bezerra.mcad.rmcast.SimpleReliableMulticastAgent;

public class ReliableMulticastAgentFactory {
   public static final Logger log = Logger.getLogger(MulticastAgentFactory.class);
   /* 
      
      The following method loads a multicast agent, with all its required
      internals, from a .json configuration file. The field "agent_class"
      informs this factory which specific agent implementation to build.
      The format of the .json file is the following:

      {
        "agent_class" : "MinimalMcastAgent" ,  
        (implementation specific data)
      }

    */
   public static ReliableMulticastAgent createReliableMulticastAgent(String configFile, Integer id) {
      try {
         log.setLevel(Level.OFF);
         
         log.info("Parsing the mcagent config file to create rmcast agent");
         
         JSONParser parser = new JSONParser();
         
         Object obj = parser.parse(new FileReader(configFile));         
         JSONObject config = (JSONObject) obj;         
         String rmcast_agent_type = (String) config.get("rmcast_agent_class");
         
         log.info("Reliable-multicast agent Type: " + rmcast_agent_type);
         
         if (rmcast_agent_type.equals("SimpleReliableMulticastAgent")) {
            log.info("Creating SimpleReliableMulticastAgent");
            return new SimpleReliableMulticastAgent(id, configFile);
         }
         else {
            log.error("rmcast_agent_class field in " + configFile + " didn't match any known ReliableMulticastAgent type");
         }
         
      } catch (IOException e) {
         e.printStackTrace();
      } catch (ParseException e) {
         e.printStackTrace();
      }
      
      return null;
   }
}
