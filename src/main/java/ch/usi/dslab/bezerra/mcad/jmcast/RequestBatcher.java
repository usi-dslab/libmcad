package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.netwrapper.Message;
import org.apache.commons.math3.util.Pair;

import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class RequestBatcher extends Thread {

    private JMcastClient parentClient;
    private Message currentBatch;
    private int currentBatchLength_Bytes;
    private Set<Group> currentDestinations;
    private BlockingQueue<Pair<Set<Group>, Message>> readyBatches;
    private long lastSendingTime;
    private static long clientBatchTimeout_ms;
    private static int clientBatchSize_Bytes;
    private static boolean isEnabled = true;

    public static long getClientBatchTimeout_ms() {
        return clientBatchTimeout_ms;
    }

    public static void setClientBatchTimeout_ms(long clientBatchTimeout_ms) {
        RequestBatcher.clientBatchTimeout_ms = clientBatchTimeout_ms;
    }

    public static int getClientBatchSize_Bytes() {
        return clientBatchSize_Bytes;
    }

    public static void setClientBatchSize_Bytes(int clientBatchSize_Bytes) {
        RequestBatcher.clientBatchSize_Bytes = clientBatchSize_Bytes;
    }

    public RequestBatcher(JMcastClient parentClient) {
        super("RequestBatcher");
        this.parentClient = parentClient;
        this.currentDestinations = null;
        this.currentBatch = new Message();
        this.readyBatches = new LinkedBlockingQueue<>();
        this.lastSendingTime = System.currentTimeMillis();
    }

    public static void setEnabled(boolean enabled) {
        RequestBatcher.isEnabled = enabled;
    }

    synchronized private void closeCurrentBatch() throws InterruptedException {
        Pair<Set<Group>, Message> readyBatch = new Pair<>(currentDestinations, currentBatch);
        readyBatches.put(readyBatch);
        currentBatch = new Message();
        currentDestinations = null;
        currentBatchLength_Bytes = 0;
    }

    synchronized private boolean isSendingTime() {
        if (currentBatch.count() == 0)
            return false;
        if (currentBatchLength_Bytes >= clientBatchSize_Bytes)
            return true;
        long now = System.currentTimeMillis();
        if (now - lastSendingTime >= clientBatchTimeout_ms) {
            lastSendingTime = now;
            return true;
        }
        return false;
    }

    synchronized private void checkBatchThresholds() throws InterruptedException {
        if (isSendingTime()) {
            closeCurrentBatch();
        }
    }

    synchronized public void addRequest(Set<Group> destinations, Message request) {
        if (isEnabled) {
            try {
                if (currentDestinations != null && currentDestinations.equals(destinations) == false) {
                    closeCurrentBatch();
                }
                currentBatchLength_Bytes += request.packContents();
                currentBatch.addItems(request);
                currentDestinations = destinations;
                checkBatchThresholds();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            parentClient.getParentAgent().multicast(destinations, request);
        }
    }

    @Override
    public void run() {
        if (isEnabled) {
            System.out.println("Message will be batched");
            try {
                while (true) {
                    Pair<Set<Group>, Message> nextBatch = readyBatches.poll(clientBatchTimeout_ms, TimeUnit.MILLISECONDS);
                    checkBatchThresholds();
                    if (nextBatch != null) {
                        parentClient.getParentAgent().multicast(nextBatch.getFirst(), nextBatch.getSecond());
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Message will be sent directly, not being batched");
            return;
        }
    }

}