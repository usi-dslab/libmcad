/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.mcad.*;
import ch.usi.dslab.bezerra.mcad.ridge.RidgeMulticastClient;
import ch.usi.dslab.bezerra.netwrapper.Message;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class JMcastAgent implements MulticastAgent, ReliableMulticastAgent {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JMcastAgent.class);
    static Map<Integer, JMcastGroup> processGroups = new ConcurrentHashMap();


    ReliableMulticastAgent rmcastAgent;
    int pid;
    JMcastGroup localGroup = null;
    boolean isServer;
    String paxosConfigFile = null;
    String mcastConfigFile = null;

    JMcastServer mcastServer;
    JMcastClient mcastClient;
    int count = 0;


    public JMcastAgent(String configFile, boolean isServer, int pid) {
        this.pid = pid;
        this.isServer = isServer;
        loadJMcastAgentConfig(configFile);


        if (isServer) {
            localGroup = getProcessGroup(pid);
            setLocalGroup(localGroup);
            paxosConfigFile = paxosConfigFile + "." + localGroup.getId();
            this.mcastServer = new JMcastServer(this, localGroup.getId(), JMcastNode.getNode(pid).getGid(), pid, mcastConfigFile, paxosConfigFile);
            rmcastAgent = ReliableMulticastAgentFactory.createReliableMulticastAgent(configFile, pid);
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mcastClient = new JMcastClient(this, pid, mcastConfigFile);
//        this.mcastClient = new JMcastClient(this, pid, mcastConfigFile);

    }


    public void loadJMcastAgentConfig(String filename) {
        try {

            JSONParser parser = new JSONParser();
            Object nodeObj = parser.parse(new FileReader(filename));
            JSONObject config = (JSONObject) nodeObj;

            paxosConfigFile = (String) config.get("paxos_config_file");
            mcastConfigFile = (String) config.get("jmcast_config_file");
            // Option to disable batching

            if (config.containsKey("batching_enabled")) {
                boolean batchingEnabled = (Boolean) config.get("batching_enabled");
                RequestBatcher.setEnabled(batchingEnabled);
            }
            if (config.containsKey("client_batch_size_threshold_bytes")) {
                int clientBatchSizeThreshold = getJSInt(config, "client_batch_size_threshold_bytes");
                RequestBatcher.setClientBatchSize_Bytes(clientBatchSizeThreshold);
            }

            if (config.containsKey("client_batch_time_threshold_ms")) {
                int clientBatchTimeThreshold = getJSInt(config, "client_batch_time_threshold_ms");
                RequestBatcher.setClientBatchTimeout_ms(clientBatchTimeThreshold);
            }


            // ===========================================
            // Creating Nodes
            JSONArray groupMembersArray = (JSONArray) config.get("group_members");
            Iterator<Object> it_groupMember = groupMembersArray.iterator();
            Group.changeGroupImplementationClass(JMcastGroup.class);
            while (it_groupMember.hasNext()) {
                JSONObject gmnode = (JSONObject) it_groupMember.next();

                int pid = getJSInt(gmnode, "pid");
                int gpid = getJSInt(gmnode, "gpid");
                int gid = getJSInt(gmnode, "group");
                String host = (String) gmnode.get("host");
                int port = getJSInt(gmnode, "port");

                JMcastGroup rgroup = (JMcastGroup) Group.getOrCreateGroup(gid);
                JMcastNode node = new JMcastNode(pid, gpid);
                node.setAddress(host);
                node.setPort(port);
                rgroup.addNode(node);
                processGroups.put(pid, rgroup);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void multicast(int groupId, Message message) {
        int[] dests = new int[1];
        dests[0] = groupId;
        multicast(dests, message);
    }

    public void multicast(int[] destinations, Message message) {
        mcastClient.getJNIClient().multicast(destinations, message);
    }


    @Override
    public void multicast(Group single_destination, Message message) {
        Set<Group> destinations = new HashSet<>(1);
        destinations.add(single_destination);
        multicast(destinations, message);
    }

    @Override
    public void multicast(Set<Group> destinations, Message message) {
        int[] dest = new int[destinations.size()];
        int i = 0;
        for (Group group : destinations) {
            dest[i++] = group.getId();
        }
        multicast(dest, message);

    }


    @Override
    public Message deliverMessage() {
        return mcastServer.deliverMessage();
    }


    public static JMcastGroup getProcessGroup(int pid) {
        return processGroups.get(pid);
    }

    public void setLocalGroup(JMcastGroup g) {
        this.localGroup = g;
    }

    @Override
    public Group getLocalGroup() {
        return this.localGroup;
    }

    @Override
    public void notifyCheckpointMade(DeliveryMetadata lastDelivery) {

    }

    @Override
    public boolean provideMulticastCheckpoint(DeliveryMetadata checkpoint) {
        return false;
    }

    @Override
    public void reliableMulticast(Group single_destination, Message message) {
        rmcastAgent.reliableMulticast(single_destination, message);
    }

    @Override
    public void reliableMulticast(List<Group> destinations, Message message) {
        rmcastAgent.reliableMulticast(destinations, message);
    }

    @Override
    public Message reliableDeliver() {
        return rmcastAgent.reliableDeliver();
    }

    static int getJSInt(JSONObject jsobj, String fieldName) {
        return ((Long) jsobj.get(fieldName)).intValue();
    }

    public JMcastClient getJMcastClient() {
//        if (mcastClient == null) initMcastClient();
        return this.mcastClient;
    }

    public JMcastServer getJMcastServer() {
        return this.mcastServer;
    }
}
