/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.mcad.Group;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JMcastGroup extends Group {

    static ArrayList<JMcastGroup> groupList;

    static {
        groupList = new ArrayList<>();
    }

    ArrayList<JMcastNode> nodeList;

    public JMcastGroup(int id) {
        super(id);
        nodeList = new ArrayList<>();
        groupList.add(this);
    }

    public void addNode(JMcastNode node) {
        nodeList.add(node);
    }

    @Override
    public List<Integer> getMembers() {
        return nodeList.stream().map(jMcastNode -> jMcastNode.getId()).collect(Collectors.toList());
    }
}
