package ch.usi.dslab.bezerra.mcad.rmcast;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RmcastHostInfo {
   String host;
   int port;
   static Map<Integer, RmcastHostInfo> rmcastHostsInfo = new ConcurrentHashMap<Integer, RmcastHostInfo>();

   private RmcastHostInfo(String host, int port) {
      this.host = host;
      this.port = port;
   }
   
   public static RmcastHostInfo getInfo(int id) {
      return rmcastHostsInfo.get(id);
   }

   public static void saveInfo(int id, String host, int port) {
      rmcastHostsInfo.put(id, new RmcastHostInfo(host, port));
   }
   
   public String getHost() {
      return host;
   }
   
   public int getPort() {
      return port;
   }
}
