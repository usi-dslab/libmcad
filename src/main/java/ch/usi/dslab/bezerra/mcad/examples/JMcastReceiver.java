package ch.usi.dslab.bezerra.mcad.examples;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.jmcast.JMcastAgent;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * Created by longle on 31.10.16.
 */
public class JMcastReceiver {
    JMcastAgent mcastAgent;
    int pid;
    int groupId;
    ThroughputPassiveMonitor tpMonitor = null;

    public JMcastReceiver(String configFile, int pid) {
        this.pid = pid;
        mcastAgent = new JMcastAgent(configFile, true, pid);
        tpMonitor = new ThroughputPassiveMonitor(pid, "jmcast_receiver", false);
        this.groupId = mcastAgent.getLocalGroup().getId();
        Thread conservativeDelivererThread = new Thread(new ConservativeDeliverer(this), "ConservativeDeliverer");
        conservativeDelivererThread.start();

    }

    public static void main(String[] args) {
        int pid = Integer.parseInt(args[0]);
        String configFile = args[1];

        String gathererHost = args[2];
        int gathererPort = Integer.parseInt(args[3]);
        int duration = Integer.parseInt(args[4]);
        String gathererLocation = args[5];

        DataGatherer.setDuration(duration);
        DataGatherer.enableFileSave(gathererLocation);
        DataGatherer.enableGathererSend(gathererHost, gathererPort);

        JMcastReceiver app = new JMcastReceiver(configFile, pid);
    }

    public static class ConservativeDeliverer implements Runnable {

        ConservativeUnbatcher unBatcher;
        JMcastReceiver app;

        public ConservativeDeliverer(JMcastReceiver app) {
            this.app = app;
            this.unBatcher = new ConservativeUnbatcher(app);
        }

        int i = 0;
        Random ran = new Random();

        @Override
        public void run() {

            while (true) {
                Message msg = unBatcher.nextMessage();
                int clientId = (Integer) msg.getNext();
                String text = (String) msg.getNext();
//                System.out.println(String.format("[%d]delivered message \"%s\"", app.pid, text.substring(0, 16)));
                app.tpMonitor.incrementCount();
                if (app.pid == 0)
                    app.mcastAgent.getJMcastServer().sendReply(clientId, new Message(text));
            }


        }

        public static class ConservativeUnbatcher {
            Queue<Message> readyMessages;
            JMcastReceiver app;

            public ConservativeUnbatcher(JMcastReceiver app) {
                readyMessages = new LinkedList<>();
                this.app = app;
            }

            public Message nextMessage() {
                if (!readyMessages.isEmpty()) {
                    return readyMessages.remove();
                } else {
                    Message m = app.mcastAgent.deliverMessage();
                    if (m.getItem(0) instanceof ClientMessage) {
                        // unbatch clientmessage batch
                        m.rewind();
                        while (m.hasNext()) {
                            Message oneMessage = (Message) m.getNext();
                            oneMessage.unpackContents();
                            readyMessages.add(oneMessage);
                        }
                        return readyMessages.remove();
                    } else {
                        return m;
                    }
                }
            }
        }

    }
}
