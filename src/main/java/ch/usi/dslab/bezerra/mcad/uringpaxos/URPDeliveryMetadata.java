package ch.usi.dslab.bezerra.mcad.uringpaxos;

import ch.usi.da.paxos.api.LearnerDeliveryMetadata;
import ch.usi.dslab.bezerra.mcad.DeliveryMetadata;

public class URPDeliveryMetadata extends DeliveryMetadata {
   private static final long serialVersionUID = 7355565506510035393L;

   LearnerDeliveryMetadata prevLearnerDeliveryMetadata;
   LearnerDeliveryMetadata lastLearnerDeliveryMetadata;
   Long batchValueCount;

   public URPDeliveryMetadata(){
   }

   public URPDeliveryMetadata(LearnerDeliveryMetadata prevmd, LearnerDeliveryMetadata lastmd, long batchValueCount) {
      this.prevLearnerDeliveryMetadata = prevmd;
      this.lastLearnerDeliveryMetadata = lastmd;
      this.batchValueCount = new Long(batchValueCount);
   }
   
   public LearnerDeliveryMetadata getPreviousLearnerDeliveryMetadata() {
      return prevLearnerDeliveryMetadata;
   }

   public void setPreviousLearnerDeliveryMetadata(LearnerDeliveryMetadata learnerDeliveryMetadata) {
      this.prevLearnerDeliveryMetadata = learnerDeliveryMetadata;
   }
   
   public LearnerDeliveryMetadata getLastLearnerDeliveryMetadata() {
      return lastLearnerDeliveryMetadata;
   }

   public void setLastLearnerDeliveryMetadata(LearnerDeliveryMetadata learnerDeliveryMetadata) {
      this.lastLearnerDeliveryMetadata = learnerDeliveryMetadata;
   }
   
   public long getBatchValueCount() {
      return batchValueCount.longValue();
   }
   
   public void setBatchValueCount(long v) {
      this.batchValueCount = new Long(v);
   }

   @Override
   public boolean precedes(DeliveryMetadata o) {
      URPDeliveryMetadata other = (URPDeliveryMetadata) o;
      return this.compareTo(other) < 0;
   }

   @Override
   public int compareTo(DeliveryMetadata o) {
      URPDeliveryMetadata other = (URPDeliveryMetadata) o;
      if (this.lastLearnerDeliveryMetadata.compareTo(other.lastLearnerDeliveryMetadata) < 0)
         return -1;
      else if (this.lastLearnerDeliveryMetadata.compareTo(other.lastLearnerDeliveryMetadata) > 0)
         return  1;
      else { // if (this.lastLearnerDeliveryMetadata.compareTo(other.lastLearnerDeliveryMetadata) == 0)
         return this.batchValueCount.compareTo(other.batchValueCount);
      }
   }

   @Override
   public boolean equals(Object o) {
      URPDeliveryMetadata other = (URPDeliveryMetadata) o;
      return this.compareTo(other) == 0;
   }

   @Override
   public int hashCode() {
      return batchValueCount.intValue() ^ lastLearnerDeliveryMetadata.hashCode();
   }
   
   @Override
   public String toString() {
      return String.format("URPDeliveryMetadata: %d@%s...%s", batchValueCount, lastLearnerDeliveryMetadata, prevLearnerDeliveryMetadata);
   }

}
