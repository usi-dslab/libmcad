package ch.usi.dslab.bezerra.mcad.examples;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.jmcast.JMcastAgent;
import ch.usi.dslab.bezerra.netwrapper.Message;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by longle on 31.10.16.
 */
public class JMcastSender extends Thread {
    JMcastAgent mcastAgent;
    final int MSG_SIZE = 1000;
    final int GROUP_COUNT = 3;
    final Random random = new Random(); // Or SecureRandom
    final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static int numTarget = -1;
    long sentTime;
    SecureRandom rnd = new SecureRandom();
    //    ThroughputPassiveMonitor tpMonitor = null;
//    LatencyPassiveMonitor latencyMonitor;
    String randomString = randomString(MSG_SIZE);
    Set<Group> destinations = new HashSet<>();
    int clientId;

    public JMcastSender(int clientId, String configFile) {
        this.clientId = clientId;
        mcastAgent = new JMcastAgent(configFile, false, clientId);

        for (int i = 0; i < numTarget; i++) {
            destinations.add(Group.getGroup(i));
        }
//        latencyMonitor = new LatencyPassiveMonitor(clientId, "jmcast_client");
//        tpMonitor = new ThroughputPassiveMonitor(clientId, "jmcast_client");
        Thread receiverThread = new Thread(this, "Receiver-" + 1);
        receiverThread.start();
    }

    String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static void main(String[] args) {
        int clientId = Integer.parseInt(args[0]);
        String configFile = args[1];
        numTarget = Integer.parseInt(args[2]);
        JMcastSender app = new JMcastSender(clientId, configFile);
        try {
            app.runAutomatic(20, true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void runAutomatic(int duration, boolean global) throws InterruptedException {
        while (!mcastAgent.getJMcastClient().isReady) Thread.sleep(100);
        Set<Group> destinations = new HashSet<>();
        destinations.add(Group.getGroup(0));
        if (global) destinations.add(Group.getGroup(1));
        System.out.println("Clien sending requests...");

        String message = randomString(random.nextInt(MSG_SIZE) + 16);
        sendMessage(message, destinations);
    }


    public void sendMessage(String text, Set<Group> destinations) {
        sentTime = System.nanoTime();
        ClientMessage clientMessage = new ClientMessage(this.clientId, text);
//        Message multicastMessage = new Message(new Msg(text + " -" + System.currentTimeMillis()));
        mcastAgent.getJMcastClient().multicast(destinations, clientMessage);
//        mcastAgent.multicast(destinations, multicastMessage);
    }


    @Override
    public void run() {
        while (true) {
            Message msg = mcastAgent.getJMcastClient().deliverReply();


//            System.out.print(String.format("delivered message \"%s\" from server. send another %s ", msg.toString().substring(0, 16), randomString.substring(0, 16)));
//            for (Group g : destinations) {
//                System.out.print(g.getId());
//            }
//            System.out.println();
            sendMessage(randomString, destinations);
        }
    }
}
