/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.jmcast;


import java.util.HashMap;

public class JMcastNode {

    static HashMap<Integer, JMcastNode> nodeMap;

    static {
        nodeMap = new HashMap<>();
    }

    public static JMcastNode getNode(int id) {
        JMcastNode node = nodeMap.get(id);
        return node;
    }


    String address;
    int id;
    int port;
    int gid;

    public JMcastNode(int id, int gid) {
        this.id = id;
        this.gid = gid;
        nodeMap.put(id, this);
    }


    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    public int getPort() {
        return port;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }
}
