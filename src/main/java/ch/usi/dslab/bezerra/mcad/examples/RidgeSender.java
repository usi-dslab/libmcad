/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

import ch.usi.dslab.bezerra.mcad.*;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;

public class RidgeSender {
    Client client;
    BufferedReader br;
    final int OUTSTANDING_PERMITS = 1;
    Semaphore outstandingPermits = new Semaphore(OUTSTANDING_PERMITS, true);
    final int MSG_SIZE = 56; // bytes
    byte[] request = new byte[MSG_SIZE];
    String message = request.toString();
    static boolean global = false;
    static boolean interactive = false;
    static long sentTime;

    public RidgeSender(int senderId, String configFile) {
        client = new Client(senderId, configFile);
    }

    public String askForInput() throws IOException {
        if (br == null) br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("multicast message m to g1, to g2 or to g1 & g2. (examples: \"MSG 1 2\", \"m 1\", \"Message 2\"):");
        String input = br.readLine();
        return input;
    }

    void runAutomatic(int duration, boolean global) {
        Set<Group> destinations = new HashSet<>();
        destinations.add(Group.getGroup(1));
        if (global) destinations.add(Group.getGroup(2));
        System.out.println("Clien sending requests...");
        client.sendMessage(message, destinations);
    }

    public void sendBurst() {
        int numMessages = 100;
        for (int i = 0; i < numMessages; i++) {
            String text = String.format("msg_%d", i);
            Set<Group> destinations = new HashSet<>();
            destinations.add(Group.getGroup(1));
            destinations.add(Group.getGroup(2));
            client.sendMessage(text, destinations);
        }
    }

    public static void main(String[] args) throws IOException {
        String configFile = args[0];
        int senderId = Integer.parseInt(args[1]);
        interactive = Boolean.parseBoolean(args[2]);
        global = Boolean.parseBoolean(args[3]);

        String gathererHost = args[4];
        int gathererPort = Integer.parseInt(args[5]);
        int duration = Integer.parseInt(args[6]);
        String gathererLocation = args[7];
        DataGatherer.setDuration(duration);
        DataGatherer.enableFileSave(gathererLocation);
        DataGatherer.enableGathererSend(gathererHost, gathererPort);


        RidgeSender sender = new RidgeSender(senderId, configFile);
        if (interactive) {
            String input = sender.askForInput();

            while (input.equalsIgnoreCase("end") == false) {

                if (input.equalsIgnoreCase("burst"))
                    sender.sendBurst();
                else {
                    String[] params = input.split(" ");
                    String message = params[0];
                    Set<Group> destinationGroups = new HashSet<>();
                    for (int i = 1; i < params.length; i++) {
                        int groupId = Integer.parseInt(params[i]);
                        destinationGroups.add(Group.getGroup(groupId));
                    }
                    sender.client.sendMessage(message, destinationGroups);
                }

                input = sender.askForInput();
            }
        } else {
            sender.runAutomatic(20, global);

        }
    }


    class Client implements Runnable {
        MulticastAgent mcagent;
        ThroughputPassiveMonitor tpMonitor = null;

        LatencyPassiveMonitor latencyMonitor;
        MulticastClient multicastClient;
        private Thread clientThread;

        public Client(int senderId, String configFile) {
//            mcagent = MulticastAgentFactory.createMulticastAgent(configFile, false, senderId);
            multicastClient = MulticastClientServerFactory.getClient(senderId, configFile);
            multicastClient.connectToOneServerPerPartition();
            latencyMonitor = new LatencyPassiveMonitor(senderId, "client");
            tpMonitor = new ThroughputPassiveMonitor(senderId, "client");
            clientThread = new Thread(this, "ClientThread-" + senderId);
            clientThread.start();
        }


        public void sendMessage(String text, Set<Group> destinations) {
            sentTime = System.nanoTime();
//            System.out.println(text + " - " + destinations.size());
            Message multicastMessage = new Message(text, System.currentTimeMillis());
            ClientMessage clientMessage = new ClientMessage(multicastMessage);
            multicastClient.multicast(destinations, clientMessage);
        }


        @Override
        public void run() {
            while (true) {
                Message replyWrapper = multicastClient.deliverReply();
//                outstandingPermits.release();
                String reply = (String) replyWrapper.getItem(0);
                long now = System.nanoTime();
                latencyMonitor.logLatency(sentTime, now);
                tpMonitor.incrementCount();
                if (interactive)
                    System.out.println(reply);
                if (!interactive) {
                    Set<Group> destinations = new HashSet<>();
                    destinations.add(Group.getGroup(1));
                    if (global) destinations.add(Group.getGroup(2));
                    byte[] request = new byte[MSG_SIZE];
                    String message = request.toString();
                    sendMessage(message, destinations);
                }
            }
        }
    }
}
