package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.netwrapper.Message;

/**
 * Created by longle on 21.03.17.
 */
public class JMcastMessage extends Message {
    public JMcastMessage() {
    }

    public JMcastMessage(Object... objects) {
        super(objects);
    }
}
