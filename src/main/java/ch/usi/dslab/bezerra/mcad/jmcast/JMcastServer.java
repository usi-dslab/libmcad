/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.jmcast;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.MulticastAgent;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.NodeCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastNodeJNI;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//import ch.usi.dslab.lel.libjmcast.ClientCallbackHandler;
//import ch.usi.dslab.lel.libjmcast.jni.McastClientJNI;

public class JMcastServer implements MulticastServer {


    public static enum MessageType {
        CLIENT_CREDENTIALS
    }

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JMcastServer.class);
    private int serverId;
    private int localId;
    private int groupId;
    private String mcastConfig;
    private String paxosConfig;

    private MulticastAgent parentAgent;
    Queue<ClientMessage> unbatchedClientMessages;
    Thread receiverThread;
    LinkedList<Message> lst = new LinkedList<>();
    //    Receiver receiver;
    public McastNodeJNI mcastNode;
    public BlockingQueue<Message> deliveryQueue;

    public JMcastServer(MulticastAgent parentAgent, int groupId, int localId, int serverId, String mcastConfig, String paxosConfig) {
        this.serverId = serverId;
        this.localId = localId;
        this.groupId = groupId;
        this.parentAgent = parentAgent;
        this.mcastConfig = mcastConfig;
        this.paxosConfig = paxosConfig;
        this.unbatchedClientMessages = new LinkedList<>();
        this.deliveryQueue = new LinkedBlockingQueue<>();

        mcastNode = new McastNodeJNI(groupId, localId, mcastConfig, paxosConfig);
        NodeCallbackHandler cbOnDeliver = (clientId, reply) -> {
//            synchronized (lst){
//                lst.add(reply);
//                lst.notify();
//            }
            if (reply instanceof JMcastMessage) {
                System.out.println(groupId+" receiv auth msg from client " + clientId);
                return;
            }
            try {
                deliveryQueue.put(reply);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println(reply.toString().substring(1, 16) + "-" + Message.getBytes(reply).length);
//            ((JMcastAgent) parentAgent).enqueueMessageForDelivery(reply);

//            this.sendReply(1, new Message("%%" + "12341212341234312423" + "%%"));
        };
        mcastNode.setCallback(cbOnDeliver);
        Thread receiverThread = new Thread(mcastNode, "JMCastNodeJNI-" + this.groupId + "-" + this.localId);
        receiverThread.start();
    }

    public int getLocalId() {
        return this.localId;
    }

    @Override
    public int getId() {
        return this.serverId;
    }

    @Override
    public boolean isConnectedToClient(int clientId) {
        return mcastNode.isConnectedToClient(clientId);
    }

    @Override
    public ClientMessage deliverClientMessage() {
        if (unbatchedClientMessages.isEmpty()) {
            Message nextClientRequestBatch = this.parentAgent.deliverMessage();
            nextClientRequestBatch.rewind();
            while (nextClientRequestBatch.hasNext()) {
                ClientMessage clireq = (ClientMessage) nextClientRequestBatch.getNext();
                clireq.unpackContents();
                unbatchedClientMessages.add(clireq);
            }
        }

        ClientMessage climsg = unbatchedClientMessages.remove();
        return climsg;
    }

    @Override
    public void sendReply(int clientId, Message reply) {
        mcastNode.reply(clientId, reply);
    }

    @Override
    public MulticastAgent getMulticastAgent() {
        return this.parentAgent;
    }


    public Message deliverMessage() {
//        synchronized (lst){
//            if (lst.size()==0) try {
//                lst.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return lst.removeFirst();
//        }
        Message msg = null;
        try {
            msg = deliveryQueue.take();
            msg.rewind();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return msg;
    }
}
