/*

 Libmcad - A multicast adaptor library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Libmcad.
 
 Libmcad is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Libmcad is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.mcad.examples;

import ch.usi.da.paxos.lab.Multicast;
import ch.usi.dslab.bezerra.mcad.MulticastAgent;
import ch.usi.dslab.bezerra.mcad.MulticastAgentFactory;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;

public class RidgeReceiver extends Thread {

    //    MulticastAgent mcagent;
    MulticastServer mcServer;
    ThroughputPassiveMonitor tpMonitor = null;
    int nodeId;
    int groupId;
    static int partitions;


    public RidgeReceiver(int nodeId, int groupId, String configFile) {
//        mcagent = MulticastAgentFactory.createMulticastAgent(configFile, true, nodeId);
        mcServer = MulticastClientServerFactory.getServer(nodeId, configFile);
        tpMonitor = new ThroughputPassiveMonitor(nodeId, "ridge_learner");
        this.nodeId = nodeId;
        this.groupId = groupId;
    }

    @Override
    public void run() {
        while (true) {
            Message msgWrapper = mcServer.deliverClientMessage();
            Message msgContent = (Message) msgWrapper.getNext();
            tpMonitor.incrementCount();
            if (msgContent.peekNext() instanceof String) {
                String text = (String) msgContent.getNext();
//                System.out.println(String.format("server %d delivered message \"%s\" %d", nodeId, text, tpMonitor.getCount()));
                if (mcServer.isConnectedToClient(1) && groupId % partitions == 1)
                    mcServer.sendReply(1, new Message(text + " updated"));
            }
        }
    }

    public static void main(String[] args) {
      /*

       one of the receivers should start with parameters:  9 1
         the other receiver should start with parameters: 10 2

       The first parameter is the node id, and the second one is the group id to which the node belongs.
       Such node must be in the ridge configuration file, under the *group_members* section. This
       means that (for sentTime) the whole system configuration is static, given in the config file.

      */

        int nodeId = Integer.parseInt(args[0]);
        int groupId = Integer.parseInt(args[1]);
        String configFile = args[2];
        partitions = Integer.parseInt(args[3]);
        String gathererHost = args[4];
        int gathererPort = Integer.parseInt(args[5]);
        int duration = Integer.parseInt(args[6]);
        String gathererLocation = args[7];
        DataGatherer.setDuration(duration);
        DataGatherer.enableFileSave(gathererLocation);
        DataGatherer.enableGathererSend(gathererHost, gathererPort);
        Receiver receiver = new Receiver(nodeId, groupId, configFile);
        receiver.start();
    }

}
