#!/usr/bin/env bash

CONF=/home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json

CLIENT_COUNT=50
DESTINATION_COUNT=2
TIME=30
LOG_DIR=/home/long/apps/ScalableSMR/libmcad/logs/c_
LOG_DIR+=$CLIENT_COUNT
LOG_DIR+=_p_
LOG_DIR+=$DESTINATION_COUNT

JAVA_CMD="java -XX:+UseG1GC -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libmcad/bin/log4jDebug.xml -cp"
CLASSPATH="/home/long/apps/ScalableSMR/libmcad/../dependencies/*:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/../libjmcast/target/classes:/home/long/apps/ScalableSMR/libmcad/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/target/classes"
LD=LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so

python /home/long/apps/ScalableSMR/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/jmcast/JMcastPaxosDeployer.py $CONF

$JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.sense.DataGatherer 40000 $LOG_DIR throughput jmcast_receiver 3 &

ssh -o StrictHostKeyChecking=no 192.168.3.6 $LD $JAVA_CMD "$CLASSPATH"  ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 0 $CONF node1 40000 30 $LOG_DIR &
ssh -o StrictHostKeyChecking=no 192.168.3.7 $LD $JAVA_CMD "$CLASSPATH"  ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 1 $CONF node1 40000 30 $LOG_DIR &
ssh -o StrictHostKeyChecking=no 192.168.3.8 $LD $JAVA_CMD "$CLASSPATH"  ch.usi.dslab.bezerra.mcad.examples.JMcastReceiver 2 $CONF node1 40000 30 $LOG_DIR &

sleep 3

id=1000
for ((i=1; i<=$CLIENT_COUNT; i++)); do
  let b=$i+8
  let id=$id+1
#  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so java -XX:+UseG1GC -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libmcad/bin/log4jDebug.xml -cp "/home/long/apps/ScalableSMR/libmcad/../dependencies/*:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/../libjmcast/target/classes:/home/long/apps/ScalableSMR/libmcad/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/target/classes" ch.usi.dslab.bezerra.mcad.examples.JMcastSender 1000 /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json 1 &
#java -XX:+UseG1GC -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libmcad/bin/log4jDebug.xml -cp "/home/long/apps/ScalableSMR/libmcad/../dependencies/*:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/../libjmcast/target/classes:/home/long/apps/ScalableSMR/libmcad/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libmcad/../sense/target/classes:/home/long/apps/ScalableSMR/libmcad/target/classes" ch.usi.dslab.bezerra.mcad.examples.JMcastSender 1000 /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json 1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.mcad.examples.JMcastSender $id /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json $DESTINATION_COUNT &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.mcad.examples.JMcastSender $id /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json $DESTINATION_COUNT &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.mcad.examples.JMcastSender $id /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json $DESTINATION_COUNT &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.mcad.examples.JMcastSender $id /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json $DESTINATION_COUNT &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.mcad.examples.JMcastSender $id /home/long/apps/ScalableSMR/libmcad/bin/system_config/cluster/generatedSysConfig.json $DESTINATION_COUNT &
done
